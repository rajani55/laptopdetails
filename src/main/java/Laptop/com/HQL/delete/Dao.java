package Laptop.com.HQL.delete;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Laptop.com.Latop_details.create.LaptopDto;

public class Dao {

	public void delete_Details(Long id)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="delete LaptopDto where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int update = query.executeUpdate();
		transaction.commit();
		if(update==0)
		{
			System.out.println("delete failed");
			return;
		}
		System.out.println("sucessful");
	}
}
