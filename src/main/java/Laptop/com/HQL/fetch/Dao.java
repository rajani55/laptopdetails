package Laptop.com.HQL.fetch;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Laptop.com.Latop_details.create.LaptopDto;

public class Dao {

	public List fetch_details() 
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String qry="from LaptopDto";
		Query query = session.createQuery(qry);
		List list = query.list();
		return list;
	}
	
}
