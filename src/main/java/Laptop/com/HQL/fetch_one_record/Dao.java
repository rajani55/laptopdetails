package Laptop.com.HQL.fetch_one_record;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import Laptop.com.Latop_details.create.LaptopDto;

public class Dao {
	
	public LaptopDto fecth_one_record(String Laptop_name)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from LaptopDto where Laptop_name=:refname";
		Query query = session.createQuery(hql);
	    query.setParameter("refname", Laptop_name);
	    LaptopDto ref =(LaptopDto)query.uniqueResult();
	    return ref;
	   
		
	}

}
