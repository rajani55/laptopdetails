package Laptop.com.HQL.update;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Laptop.com.Latop_details.create.LaptopDto;

public class Dao {
	
	public void update_Details(Long id,String price)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="update LaptopDto set price=:price where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id",id);
		query.setParameter("price", price);
		int update = query.executeUpdate();
		transaction.commit();
		if(update==0)
		{
			System.out.println("update failed");
			return;
		}
		System.out.println("update operation successful");
		
	}

}
