package Laptop.com.Latop_details.create;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Laptop_details")
public class LaptopDto implements Serializable{
	@Id
	@Column(name="id")
	private Long id;
	@Column(name="Laptop_name")
	private String Laptop_name;
	@Column(name="price")
	private String price;
	@Column(name="operating_system")
	private String operating_system;
	
	public LaptopDto() {
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLaptop_name() {
		return Laptop_name;
	}
	public void setLaptop_name(String laptop_name) {
		Laptop_name = laptop_name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getOperating_system() {
		return operating_system;
	}
	public void setOperating_system(String operating_system) {
		this.operating_system = operating_system;
	}
	@Override
	public String toString() {
		return "LaptopDto [id=" + id + ", Laptop_name=" + Laptop_name + ", price=" + price + ", operating_system="
				+ operating_system + "]";
	}
	

	
}
