package Laptop.com.Latop_details.Fetch;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Laptop.com.Latop_details.create.LaptopDto;

public class LaptopFetch {
	public LaptopDto getDetails(Long id)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		return session.get(LaptopDto.class, id);
	}

}
