package Laptop.com.Latop_details.update;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Laptop.com.Latop_details.Fetch.LaptopFetch;
import Laptop.com.Latop_details.create.LaptopDto;

public class LaptopDao {
	
	public void update_Details(Long id,String price)
	{
		LaptopFetch obj=new LaptopFetch();
		LaptopDto details = obj.getDetails(id);
		if(details!=null) 
		{
		Configuration configuration = new Configuration();
		configuration.configure();
		configuration.addAnnotatedClass(LaptopDto.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		details.setPrice(price);
		session.update(details);
		transaction.commit();
	}
		else
		{
			System.out.println("price update failed");
		}

	}
}
